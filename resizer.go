package main

type imageResizer interface {
	AllDoneInResize() bool

	Load(data []byte) (interface{}, error)
	SetQuality(data interface{}, quality int)
	Resize(data interface{}, width, height int) (interface{}, error)
	Save(data interface{}) (int, error)
	Done(data interface{})
}
