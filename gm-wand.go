package main

// #cgo pkg-config: GraphicsMagickWand
// #include <stdlib.h>
// #include <stdio.h>
// #include <string.h>
// #include <wand/magick_wand.h>
import "C"
import (
	"errors"
	"os"
	"unsafe"
)

type gmWandImage struct {
	wand *C.MagickWand
}

type gmWandResizer struct {
	strip bool
}

func init() {
	_, err := C.InitializeMagick(C.CString(os.Args[0]))
	if err != nil {
		panic(err)
	}
}

func (b *gmWandResizer) AllDoneInResize() bool {
	return false
}

func (b *gmWandResizer) Load(imageData []byte) (interface{}, error) {
	wand, err := C.NewMagickWand()
	if err != nil {
		return nil, err
	}

	if C.MagickReadImageBlob(wand, (*C.uchar)(&imageData[0]), C.ulong(len(imageData))) != C.MagickPass {
		C.DestroyMagickWand(wand)
		return nil, errors.New("failed")
	}

	if b.strip {
		// Strip image from profiles and exif
		C.MagickStripImage(wand)
	}

	return &gmWandImage{wand}, nil
}

func (b *gmWandResizer) SetQuality(data interface{}, quality int) {
	img := data.(*gmWandImage)
	C.MagickSetCompressionQuality(img.wand, C.ulong(quality))
}

func (b *gmWandResizer) Resize(data interface{}, width, height int) (interface{}, error) {
	img := data.(*gmWandImage)

	new := &gmWandImage{
		wand: C.CloneMagickWand(img.wand),
	}

	if C.MagickResizeImage(new.wand, C.ulong(width), C.ulong(height), C.LanczosFilter, 0.0) != C.MagickPass {
		b.Done(new)
		return nil, errors.New("failed")
	}
	return new, nil
}

func (b *gmWandResizer) Save(data interface{}) (int, error) {
	var size C.size_t
	img := data.(*gmWandImage)
	result := C.MagickWriteImageBlob(img.wand, &size)
	if result == nil {
		return 0, errors.New("write image blob failed")
	}

	C.MagickFree(unsafe.Pointer(result))
	return int(size), nil
}

func (b *gmWandResizer) Done(data interface{}) {
	img := data.(*gmWandImage)

	if img.wand != nil {
		C.DestroyMagickWand(img.wand)
		img.wand = nil
	}
}
