package main

import (
	"bytes"
	"image"
	"image/png"

	"github.com/disintegration/imaging"
)

type disImage struct {
	img     image.Image
	format  string
	quality int
}
type disResizer struct{}

func pngCompressionLevel(quality int) png.CompressionLevel {
	return png.CompressionLevel(quality / 10)
}

func (b *disResizer) AllDoneInResize() bool {
	return false
}

func (b *disResizer) Load(data []byte) (interface{}, error) {
	m, fmt, err := image.Decode(bytes.NewReader(data))
	return &disImage{img: m, format: fmt, quality: 60}, err
}

func (b *disResizer) SetQuality(data interface{}, quality int) {
	img := data.(*disImage)
	img.quality = quality
}

func (b *disResizer) Resize(data interface{}, width, height int) (interface{}, error) {
	img := data.(*disImage)
	new := imaging.Resize(img.img, width, height, imaging.Lanczos)
	return &disImage{img: new, format: img.format, quality: img.quality}, nil
}

func (b *disResizer) Save(data interface{}) (int, error) {
	img := data.(*disImage)
	format, err := imaging.FormatFromExtension(img.format)
	if err != nil {
		return 0, err
	}

	var buffer bytes.Buffer
	err = imaging.Encode(
		&buffer,
		img.img,
		format,
		imaging.JPEGQuality(img.quality),
		imaging.PNGCompressionLevel(pngCompressionLevel(img.quality)),
	)
	return buffer.Len(), err
}

func (b *disResizer) Done(data interface{}) {
}
