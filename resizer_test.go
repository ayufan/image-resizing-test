package main

import (
	"io/ioutil"
	"strconv"
	"testing"

	"github.com/stretchr/testify/require"
)

func load(path string) []byte {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}
	return data
}

var resizers = map[string]imageResizer{
	"bimg": &bimgResizer{},
	"nfnt": &nfntResizer{},
	"disi": &disResizer{},
	"gmco": &gmResizer{strip: true},
	"wand": &gmWandResizer{strip: true},
}

var images = map[string][]byte{
	"gitlab_small.jpg": load("data/gitlab_small.jpg"),
	"gitlab_small.png": load("data/gitlab_small.png"),
	"gitlab_large.jpg": load("data/gitlab_large.jpg"),
	"gitlab_large.png": load("data/gitlab_large.png"),
}

var sizes = map[int]int{
	10:  200,
	40:  400,
	200: 1000,
	500: 5000,
}

var qualities = []int{
	10,
	50,
	70,
	80,
	90,
}

func forEachN(b *testing.B, fn func()) {
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		fn()
	}
}

func forEachSize(b *testing.B, fn func(b *testing.B, size, minResultSize int)) {
	for size, minResultSize := range sizes {
		b.Run("width="+strconv.Itoa(size), func(b *testing.B) {
			fn(b, size, minResultSize)
		})
	}
}

func forEachQuality(b *testing.B, fn func(b *testing.B, quality int)) {
	for _, quality := range qualities {
		b.Run("quality="+strconv.Itoa(quality), func(b *testing.B) {
			fn(b, quality)
		})
	}
}

func forEachResizer(b *testing.B, allInOne bool, fn func(b *testing.B, resizer imageResizer)) {
	for name, resizer := range resizers {
		if resizer.AllDoneInResize() && !allInOne {
			//b.Skip("test not supported, as all is done in `Resize`")
			continue
		}

		b.Run(name, func(b *testing.B) {
			fn(b, resizer)
		})
	}
}

func forEachImage(b *testing.B, fn func(b *testing.B, data []byte)) {
	for name, data := range images {
		b.Run(name, func(b *testing.B) {
			fn(b, data)
		})
	}
}

func BenchmarkImageLoad(b *testing.B) {
	forEachImage(b, func(b *testing.B, data []byte) {
		forEachResizer(b, false, func(b *testing.B, resizer imageResizer) {
			forEachN(b, func() {
				img, err := resizer.Load(data)
				require.NoError(b, err)
				defer resizer.Done(img)
			})
		})
	})
}

func BenchmarkImageResize(b *testing.B) {
	forEachImage(b, func(b *testing.B, data []byte) {
		forEachSize(b, func(b *testing.B, size, _ int) {
			forEachResizer(b, false, func(b *testing.B, resizer imageResizer) {
				img, err := resizer.Load(data)
				require.NoError(b, err)
				defer resizer.Done(img)

				forEachN(b, func() {
					newImage, err := resizer.Resize(img, size, size)
					require.NoError(b, err)

					defer resizer.Done(newImage)
				})
			})
		})
	})
}

func BenchmarkImageSave(b *testing.B) {
	forEachImage(b, func(b *testing.B, data []byte) {
		forEachSize(b, func(b *testing.B, size, minResultSize int) {
			forEachQuality(b, func(b *testing.B, quality int) {
				forEachResizer(b, false, func(b *testing.B, resizer imageResizer) {
					img, err := resizer.Load(data)
					require.NoError(b, err)
					defer resizer.Done(img)
					resizer.SetQuality(img, quality)

					newImage, err := resizer.Resize(img, size, size)
					require.NoError(b, err)
					defer resizer.Done(newImage)

					forEachN(b, func() {
						resultSize, err := resizer.Save(newImage)
						require.NoError(b, err)
						require.GreaterOrEqual(b, resultSize, minResultSize)

						b.ReportMetric(float64(resultSize), "bytes/image")
					})
				})
			})
		})
	})
}

func BenchmarkImageAll(b *testing.B) {
	forEachImage(b, func(b *testing.B, data []byte) {
		forEachSize(b, func(b *testing.B, size, minResultSize int) {
			forEachQuality(b, func(b *testing.B, quality int) {
				forEachResizer(b, true, func(b *testing.B, resizer imageResizer) {
					forEachN(b, func() {
						img, err := resizer.Load(data)
						require.NoError(b, err)
						defer resizer.Done(img)
						resizer.SetQuality(img, quality)

						newImage, err := resizer.Resize(img, size, size)
						require.NoError(b, err)
						defer resizer.Done(newImage)

						resultSize, err := resizer.Save(newImage)
						require.NoError(b, err)
						require.GreaterOrEqual(b, resultSize, minResultSize)

						b.ReportMetric(float64(resultSize), "bytes/image")
					})
				})
			})
		})
	})
}
