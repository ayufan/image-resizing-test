package main

import (
	"bytes"
	"errors"
	"image"

	"image/jpeg"
	"image/png"

	"github.com/nfnt/resize"
)

type nfntImage struct {
	img     image.Image
	format  string
	quality int
}

type nfntResizer struct{}

func (b *nfntResizer) AllDoneInResize() bool {
	return false
}

func (b *nfntResizer) Load(data []byte) (interface{}, error) {
	m, fmt, err := image.Decode(bytes.NewReader(data))
	return &nfntImage{img: m, format: fmt, quality: 60}, err
}

func (b *nfntResizer) SetQuality(data interface{}, quality int) {
	img := data.(*nfntImage)
	img.quality = quality
}

func (b *nfntResizer) Resize(data interface{}, width, height int) (interface{}, error) {
	img := data.(*nfntImage)
	m := resize.Resize(uint(width), uint(height), img.img, resize.Bilinear)
	return &nfntImage{img: m, format: img.format, quality: img.quality}, nil
}

func (b *nfntResizer) Save(data interface{}) (int, error) {
	img := data.(*nfntImage)
	var buffer bytes.Buffer
	var err error
	switch img.format {
	case "jpeg":
		err = jpeg.Encode(&buffer, img.img, &jpeg.Options{Quality: img.quality})
	case "png":
		encoder := &png.Encoder{CompressionLevel: pngCompressionLevel(img.quality)}
		err = encoder.Encode(&buffer, img.img)
	default:
		err = errors.New("unsupported")
	}
	return buffer.Len(), err
}

func (b *nfntResizer) Done(data interface{}) {
}
