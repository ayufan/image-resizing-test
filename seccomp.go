package main

import (
	"syscall"

	seccomp "github.com/seccomp/libseccomp-golang"
)

var allowedSyscalls = []string{
	"futex",
	"mmap",
	"openat",
	"read",
	"seccomp",
	"fstat",
	"close",
	"mprotect",
	"getdents64",
	"pread64",
	"brk",
	"write",
	"prctl",
	"arch_prctl",
	"stat",
	"munmap",
	"rt_sigaction",
	"rt_sigprocmask",
	"rt_sigreturn",
	"access",
	"getpid",
	"clone",
	"uname",
	"fcntl",
	"ftruncate",
	"unlink",
	"umask",
	"sigaltstack",
	"statfs",
	"mlock",
	"gettid",
	"sched_getaffinity",
	"set_tid_address",
	"readlinkat",
	"set_robust_list",
	"prlimit64",
	"getrandom",
}

func enterSeccompMode() {
	println("Entering seccomp mode")
	// create a "reject all" filter that always returns "Operation not permitted"
	filter, err := seccomp.NewFilter(seccomp.ActErrno.SetReturnCode(int16(syscall.EPERM)))
	if err != nil {
		panic(err)
	}
	// allow only syscalls in the given list
	for _, syscall := range allowedSyscalls {
		id, err := seccomp.GetSyscallFromName(syscall)
		if err != nil {
			panic(err)
		}
		filter.AddRule(id, seccomp.ActAllow)
	}
	filter.Load()
	println("Seccomp mode set")
}
