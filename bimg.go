package main

import (
	"github.com/h2non/bimg"
)

type bimgResizer struct{}

type bimgImage struct {
	data    []byte
	quality int
}

func (b *bimgResizer) AllDoneInResize() bool {
	// bimg: does everything during a resize
	return true
}

func (b *bimgResizer) Load(data []byte) (interface{}, error) {
	return &bimgImage{data: data, quality: 60}, nil
}

func (b *bimgResizer) SetQuality(data interface{}, quality int) {
	img := data.(*bimgImage)
	img.quality = quality
}

func (b *bimgResizer) Resize(data interface{}, width, height int) (interface{}, error) {
	img := data.(*bimgImage)

	options := bimg.Options{
		Width:       width,
		Height:      height,
		Embed:       true,
		Quality:     img.quality,
		Compression: int(pngCompressionLevel(img.quality)),
	}
	resized, err := bimg.Resize(img.data, options)
	return &bimgImage{data: resized, quality: img.quality}, err
}

func (b *bimgResizer) Save(data interface{}) (int, error) {
	img := data.(*bimgImage)
	return len(img.data), nil
}

func (b *bimgResizer) Done(data interface{}) {
}
