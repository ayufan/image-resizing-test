package main

// #cgo pkg-config: GraphicsMagickWand
// #include <stdlib.h>
// #include <stdio.h>
// #include <string.h>
// #include <wand/magick_wand.h>
import "C"
import (
	"errors"
	"unsafe"
)

type gmImage struct {
	image *C.Image
	info  *C.ImageInfo
}

type gmResizer struct {
	strip bool
}

func (b *gmResizer) AllDoneInResize() bool {
	return false
}

func (b *gmResizer) Load(imageData []byte) (interface{}, error) {
	img := &gmImage{}

	img.info = C.CloneImageInfo((*C.ImageInfo)(nil))
	if img.info == nil {
		return nil, errors.New("clone image info failed")
	}

	var exception C.ExceptionInfo

	C.GetExceptionInfo(&exception)
	img.image = C.BlobToImage(img.info, unsafe.Pointer(&imageData[0]), C.ulong(len(imageData)), &exception)
	C.DestroyExceptionInfo(&exception)
	if img.image == nil {
		return nil, errors.New("blob to image failed")
	}

	if b.strip {
		// Strip image from profiles and exif
		C.StripImage(img.image)
	}

	return img, nil
}

func (b *gmResizer) SetQuality(data interface{}, quality int) {
	img := data.(*gmImage)
	img.info.quality = C.ulong(quality)
}

func (b *gmResizer) Resize(data interface{}, width, height int) (interface{}, error) {
	img := data.(*gmImage)

	var exception C.ExceptionInfo

	C.GetExceptionInfo(&exception)
	newImage := C.ResizeImage(img.image, C.ulong(width), C.ulong(height), C.LanczosFilter, 0.0, &exception)
	C.DestroyExceptionInfo(&exception)
	if newImage == nil {
		return nil, errors.New("failed")
	}

	return &gmImage{
		info:  C.CloneImageInfo(img.info),
		image: newImage,
	}, nil
}

func (b *gmResizer) Save(data interface{}) (int, error) {
	img := data.(*gmImage)

	var out C.size_t
	var exception C.ExceptionInfo

	C.GetExceptionInfo(&exception)
	result := C.ImageToBlob(img.info, img.image, &out, &exception)
	C.DestroyExceptionInfo(&exception)

	if result == nil {
		return 0, errors.New("failed to save")
	}

	C.MagickFree(unsafe.Pointer(result))
	return int(out), nil
}

func (b *gmResizer) Done(data interface{}) {
	img := data.(*gmImage)

	if img.image != nil {
		C.DestroyImage(img.image)
		img.image = nil
	}
	if img.info != nil {
		C.DestroyImageInfo(img.info)
		img.info = nil
	}
}
